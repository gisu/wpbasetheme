<?php get_header(); ?>
	<?php get_template_part( 'includes/breadcrumb' , 'single'); ?>
	<div id="mainArea" class="col-9 staticPage">
		<article class="pageStyle">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>
			
			<?php wp_link_pages(array('before' => '<p><strong>'.__('Pages:','themify').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
			
		
		<?php endwhile; ?>
		</article>
	</div>
	<!-- /#content -->
		
	<?php get_sidebar(); ?>
	
<?php get_footer(); ?>