<?php if ( post_password_required() ) : ?>
	<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'themify' ); ?></p>
<?php
		/* Stop the rest of comments.php from being processed,
		 * but don't kill the script entirely -- we still have
		 * to fully load the template.
		 */
		return;
	endif;
?>

<?php
	// You can start editing here -- including this comment!
?>

<?php if ( have_comments() || comments_open() ) : ?>
<div id="comments" class="commentList">
<?php endif; // end commentwrap ?>

<?php if ( have_comments() ) : ?>
	<h3 class="icon-comment-big"><?php comments_number(__('Keine Kommentare','themify'), __('Ein Kommentar','themify'), __('% Kommentare','themify') );?></h3>

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<div class="pagenav top clearfix">
			<?php paginate_comments_links( array('prev_text' => '&laquo;', 'next_text' => '&raquo;') );?>
		</div> 
		<!-- /.pagenav -->
	<?php endif; // check for comment navigation ?>

	<ul>
		<?php wp_list_comments('callback=custom_theme_comment'); ?>
	</ul>

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
		<div class="pagenav bottom clearfix">
			<?php paginate_comments_links( array('prev_text' => '&laquo;', 'next_text' => '&raquo;') );?>
		</div>
		<!-- /.pagenav -->
	<?php endif; // check for comment navigation ?>

<?php else : // or, if we don't have comments:

	/* If there are no comments and comments are closed,
	 * let's leave a little note, shall we?
	 */
	if ( ! comments_open() ) :
?>

<?php endif; // end ! comments_open() ?>

<?php endif; // end have_comments() ?>

<?php 
$custom_comment_form = array( 'fields' => apply_filters( 'comment_form_default_fields', array(
    'author' => '<div class="formRow">' .
    		'<input id="author" name="author" type="text" placeholder="Ihr Name*" aria-required="true" class="required"/>'.
			'</div>',
    'email'  => '<div class="formRow">' .
			'<input id="email" name="email" type="email" aria-required="true" class="required" placeholder="Email* (wird nicht ver&ouml;ffentlicht)"/>'.
			'</div>',
    'url'    =>  '<div class="formRow">' .
			'<input id="url" name="url" type="url" placeholder="Webseite (Optional)"/>'.
			'</div>') ),
	'comment_field' => '<div class="formRow">' .
			'<textarea id="comment" name="comment" aria-required="true" class="required" placeholder="Kommentar*"></textarea>'.
			'</div>',
	'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Eingeloggt als <a href="%1$s">%2$s</a>. <a href="%3$s" class="smallbutton whiteButton">Ausloggen?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
	'title_reply' => 'Schreibe einen Kommentar',
	'title_reply_to' => 'Schreibe eine Antwort auf %s',
	'comment_notes_before' => '',
	'comment_notes_after' => '',
	'cancel_reply_link' => __( 'Abbruch' , 'themify' ),
	'label_submit' => __( 'Kommentar speichern' , 'themify' ),
); ?> 
 </div><div class="commentFormular">
<?php comment_form_own($custom_comment_form); 
?>

<?php if ( have_comments() || comments_open() ) : ?>
</div>
<!-- /.commentwrap -->
<?php endif; // end commentwrap ?>