<?php get_header(); ?>		
	<?php 
		if ( !is_home()) :
			get_template_part( 'includes/breadcrumb' , 'index'); 
		endif; 
	?>
	<div id="mainArea" class="col-9">
		
		<?php // the loop ?>
		<?php if (have_posts()) : ?>
		
			<?php while (have_posts()) : the_post(); ?>
	
				<?php get_template_part( 'includes/post-preview-loop' , 'index'); ?>
	
			<?php endwhile; ?>
							
			<?php get_template_part( 'includes/pagination'); ?>
		
		<?php else : ?>
			<h2>Leider noch keine Inhalte</h2>
	
		<?php endif; ?>			
	
	</div>
	<!-- /#content -->

	<?php get_sidebar(); ?>

<?php get_footer(); ?>