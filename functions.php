<?php

///////////////////////////////////////
// You may add your custom functions here
///////////////////////////////////////

	///////////////////////////////////////
	// Load theme languages
	///////////////////////////////////////
	load_theme_textdomain( 'themify', TEMPLATEPATH.'/languages' );

	///////////////////////////////////////
	// Enable WordPress feature image
	///////////////////////////////////////
	add_theme_support( 'post-thumbnails');

	///////////////////////////////////////
	// Register Custom Menu Function
	///////////////////////////////////////
	if (function_exists('register_nav_menus')) {
		register_nav_menus( array(
			'main-nav' => __( 'Main Navigation', 'themify' ),
			'soft-nav' => __( 'Soft Navigation', 'themify' ),
			'footer-nav' => __( 'Footer Navigation', 'themify' ),
		) );
	}

	///////////////////////////////////////
	// Default Main Nav Function
	///////////////////////////////////////
	function default_main_nav() {
		echo '<ul id="main-nav" class="main-nav clearfix">';
		wp_list_pages('title_li=');
		echo '</ul>';
	}

	///////////////////////////////////////
	// Register Widgets
	///////////////////////////////////////
	if ( function_exists('register_sidebar') ) {
		register_sidebar(array(
			'name' => 'Sidebar',
			'id' => 'sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="widgetHeader">',
			'after_title' => '</div>',
		));
	}

	///////////////////////////////////////
	// Page navigation
	///////////////////////////////////////
	function themify_pagenav($before = '', $after = '') {
		global $wpdb, $wp_query;
	
		$request = $wp_query->request;
		$posts_per_page = intval(get_query_var('posts_per_page'));
		$paged = intval(get_query_var('paged'));
		$numposts = $wp_query->found_posts;
		$max_page = $wp_query->max_num_pages;
	
		if(empty($paged) || $paged == 0) {
			$paged = 1;
		}
		$pages_to_show = apply_filters('themify_filter_pages_to_show', 1);
		$pages_to_show_minus_1 = $pages_to_show-1;
		$half_page_start = floor($pages_to_show_minus_1/2);
		$half_page_end = ceil($pages_to_show_minus_1/2);
		$start_page = $paged - $half_page_start;
		if($start_page <= 0) {
			$start_page = 1;
		}
		$end_page = $paged + $half_page_end;
		if(($end_page - $start_page) != $pages_to_show_minus_1) {
			$end_page = $start_page + $pages_to_show_minus_1;
		}
		if($end_page > $max_page) {
			$start_page = $max_page - $pages_to_show_minus_1;
			$end_page = $max_page;
		}
		if($start_page <= 0) {
			$start_page = 1;
		}
	
		if ($max_page > 1) {
			echo $before.'<ul class="pager clearfix">';
			if ($start_page >= 2 && $pages_to_show < $max_page) {
				$first_page_text = "&laquo;";
				echo '<li class="floatleft"><a href="'.get_pagenum_link().'" title="'.$first_page_text.'" class="button blueButton"><i class="white-arrow-left leftIcon"></i><span>'.$first_page_text.'</span></a>';
			}
			//previous_posts_link('&lt;');
			for($i = $start_page; $i  <= $end_page; $i++) {
				if($i == $paged) {
					echo ' <span class="number current">'.$i.'</span> ';
				} else {
					echo ' <a href="'.get_pagenum_link($i).'" class="number">'.$i.'</a> ';
				}
			}
			//next_posts_link('&gt;');
			if ($end_page < $max_page) {
				$last_page_text = "&raquo;";
				echo '<li class="floatright"><a href="'.get_pagenum_link().'" title="'.$first_page_text.'" class="button blueButton"><i class="white-arrow-left rightIcon"></i><span>'.$first_page_text.'</span></a>';
				echo '<a href="'.get_pagenum_link($max_page).'" title="'.$last_page_text.'" class="number">'.$last_page_text.'</a>';
			}
			echo '</ul>'.$after;
		}
	}

	///////////////////////////////////////
	// Add wmode transparent and post-video container for responsive purpose
	///////////////////////////////////////	
	function themify_add_video_wmode_transparent($html, $url, $attr) {
		
		$html = '<div class="post-video">' . $html . '</div>';
		if (strpos($html, "<embed src=" ) !== false) {
			$html = str_replace('</param><embed', '</param><param name="wmode" value="transparent"></param><embed wmode="transparent" ', $html);
			return $html;
		}
		else {
			if(strpos($html, "wmode=transparent") == false){
				if(strpos($html, "?fs=" ) !== false){
					$search = array('?fs=1', '?fs=0');
					$replace = array('?fs=1&wmode=transparent', '?fs=0&wmode=transparent');
					$html = str_replace($search, $replace, $html);
					return $html;
				}
				else{
					$youtube_embed_code = $html;
					$patterns[] = '/youtube.com\/embed\/([a-zA-Z0-9._-]+)/';
					$replacements[] = 'youtube.com/embed/$1?wmode=transparent';
					return preg_replace($patterns, $replacements, $html);
				}
			}
			else{
				return $html;
			}
		}
	}
	add_filter('embed_oembed_html', 'themify_add_video_wmode_transparent');

	///////////////////////////////////////
	// Custom Theme Comment List Markup
	///////////////////////////////////////
	function custom_theme_comment($comment, $args, $depth) {
	   $GLOBALS['comment'] = $comment; 
	   ?>

	<li id="comment-<?php comment_ID() ?>" <?php comment_class(); ?>>
		<article class="commentBlock">
		  	<?php if (function_exists('get_avatar')) : ?>
		  		<figure class="commentAvatar">
		  			<?php echo get_avatar($comment,$size='48'); ?>
		  		</figure>
			<?php endif; ?>
		  <div class="commentContent">
		    <div class="commentMeta"><?php printf('<cite>%s</cite>', get_comment_author_link()) ?> | <?php comment_date('d.m.Y'); ?>, <?php comment_time('H:i'); ?> Uhr</div>

		    <?php if ($comment->comment_approved == '0') : ?>
		    <p>
		    	<em><?php _e('Your comment is awaiting moderation.', 'themify') ?></em>
		    </p>
		    <?php endif; ?>
		    <?php comment_text() ?>

		    <nav class="commentButtons clearfix">
		      <ul class="nav pullright">
		      	<?php edit_comment_link_own('Editieren','','') ?>
		      	<?php comment_reply_link_own(array_merge( $args, array('add_below' => 'comment', 'depth' => $depth, 'reply_text' => 'Antworten', 'max_depth' => $args['max_depth']))) ?>

		      </ul>
		    </nav>
		  </div>
		</article>
	<?php
	}

	// Kick Out <BR> out of Contact Form 7
	define ('WPCF7_AUTOP', false );

	// Dynamic Excerpt
	function the_excerpt_dynamic($length) { // Outputs an excerpt of variable length (in characters)
	    global $post;
	    $text = $post->post_exerpt;
	    if ( '' == $text ) {
	    $text = get_the_content('');
	    $text = apply_filters('the_content', $text);
	    $text = str_replace(']]>', ']]>', $text);
	    }
	    $text = strip_shortcodes( $text ); // optional, recommended
	    $text = strip_tags($text,'<p><a>'); // use ' $text = strip_tags($text,'<p><a>'); ' to keep some formats; optional

	    $text = substr($text,0,$length).'...<br class="clear"><div class="clearfix"><a class="button floatright" href="'. get_permalink($post->ID) . '"><span>Weiterlesen</span></a></div>';
	    echo apply_filters('the_excerpt',$text);
	}

	// Breadcrumb
	function nav_breadcrumb() {
	 
	  $delimiter = '&raquo;';
	  $home = 'Home'; 
	  $before = '<span class="current">'; 
	  $after = '</span>'; 
	 
	  if ( !is_home() && !is_front_page() || is_paged() ) {
	 
	    echo '<div id="bread">';
	 
	    global $post;
	    $homeLink = get_bloginfo('url');
	    echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
	 
	    if ( is_category() ) {
	      global $wp_query;
	      $cat_obj = $wp_query->get_queried_object();
	      $thisCat = $cat_obj->term_id;
	      $thisCat = get_category($thisCat);
	      $parentCat = get_category($thisCat->parent);
	      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
	      echo $before . single_cat_title('', false) . $after;
	 
	    } elseif ( is_day() ) {
	      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
	      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
	      echo $before . get_the_time('d') . $after;
	 
	    } elseif ( is_month() ) {
	      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
	      echo $before . get_the_time('F') . $after;
	 
	    } elseif ( is_year() ) {
	      echo $before . get_the_time('Y') . $after;
	 
	    } elseif ( is_single() && !is_attachment() ) {
	      if ( get_post_type() != 'post' ) {
	        $post_type = get_post_type_object(get_post_type());
	        $slug = $post_type->rewrite;
	        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
	        echo $before . get_the_title() . $after;
	      } else {
	        $cat = get_the_category(); $cat = $cat[0];
	        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
	        echo $before . get_the_title() . $after;
	      }
	 
	    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
	    	if ( get_search_query() != ” ) {
	    		echo $before . 'Kein Suchergebniss f&uuml;r "' . get_search_query() . '"' . $after;
	    	} else {
		      $post_type = get_post_type_object(get_post_type());
		      echo $before . $post_type->labels->singular_name . $after;
	  		}
	 
	    } elseif ( is_attachment() ) {
	      $parent = get_post($post->post_parent);
	      $cat = get_the_category($parent->ID); $cat = $cat[0];
	      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
	      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
	      echo $before . get_the_title() . $after;
	 
	    } elseif ( is_page() && !$post->post_parent ) {
	      echo $before . get_the_title() . $after;
	 
	    } elseif ( is_page() && $post->post_parent ) {
	      $parent_id  = $post->post_parent;
	      $breadcrumbs = array();
	      while ($parent_id) {
	        $page = get_page($parent_id);
	        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
	        $parent_id  = $page->post_parent;
	      }
	      $breadcrumbs = array_reverse($breadcrumbs);
	      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
	      echo $before . get_the_title() . $after;
	 
	    } elseif ( is_search() ) {
	      echo $before . 'Ergebnisse für Ihre Suche nach "' . get_search_query() . '"' . $after;

	    } elseif ( is_tag() ) {
	      echo $before . 'Beiträge mit dem Schlagwort "' . single_tag_title('', false) . '"' . $after;
	 
	    } elseif ( is_author() ) {
	       global $author;
	      $userdata = get_userdata($author);
	      echo $before . 'Beiträge veröffentlicht von ' . $userdata->display_name . $after;
	 
	    } elseif ( is_404() ) {
	      echo $before . 'Fehler 404' . $after;
	    }
	 
	    if ( get_query_var('paged') ) {
	      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
	      echo ': ' . __('Page') . ' ' . get_query_var('paged');
	      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
	    }
	 
	    echo '</div>';
	 
	  }
	}

	// Last Post Class
	add_filter( 'post_class', 'last_post_class' );

	function last_post_class( $classes ) {
		global $wp_query;
		if( ( $wp_query->current_post + 1 ) === $wp_query->post_count )
			$classes[] = 'lastpost';
		return $classes;
	}

	// WP Use Figure
	function html5_insert_image($html, $id, $caption, $title, $align, $url) {
	    $url = wp_get_attachment_url($id);
	    $html5 = "<figure id='post-$id media-$id' class='alignresponse align$align'>";
	    $html5 .= "<img class='fluid fullsize' src='$url' alt='$title' />";
	    if ($caption) { $html5 .= "<figcaption>$caption</figcaption>";}
	    $html5 .= "</figure>";
	    return $html5;
	}
	add_filter( 'image_send_to_editor', 'html5_insert_image', 10, 9 );	// OVERWRITE CORE FEATURES

	// Own Comment Edit Button
	function edit_comment_link_own( $link = null, $before = '', $after = '' ) {
		global $comment;
		if ( !current_user_can( 'edit_comment', $comment->comment_ID ) )
		        return;
		if ( null === $link )
		        $link = __('Edit This');
		$link = '<li><a class="commentEdit" href="' . get_edit_comment_link( $comment->comment_ID ) . '" title="' . esc_attr__( 'Edit comment' ) . '"><span>' . $link . '</span></a></li>';
	        echo $before . apply_filters( 'edit_comment_link', $link, $comment->comment_ID ) . $after;
	}

	// Own Comment Reply Button
	function get_comment_reply_link_own($args = array(), $comment = null, $post = null) {
		global $user_ID;

		$defaults = array('add_below' => 'comment', 'respond_id' => 'respond', 'reply_text' => __('Reply'),
			'login_text' => __('Log in to Reply'), 'depth' => 0, 'before' => '', 'after' => '');

		$args = wp_parse_args($args, $defaults);

		if ( 0 == $args['depth'] || $args['max_depth'] <= $args['depth'] )
			return;

		extract($args, EXTR_SKIP);

		$comment = get_comment($comment);
		if ( empty($post) )
			$post = $comment->comment_post_ID;
		$post = get_post($post);

		if ( !comments_open($post->ID) )
			return false;

		$link = '';

		if ( get_option('comment_registration') && !$user_ID )
			$link = '<a rel="nofollow" class="comment-reply-login" href="' . esc_url( wp_login_url( get_permalink() ) ) . '">' . $login_text . '</a>';
		else
			$link = "<li><a class='commentResponse' href='" . esc_url( add_query_arg( 'replytocom', $comment->comment_ID ) ) . "#" . $comment->comment_ID . "' onclick='return addComment.moveForm(\"$add_below-$comment->comment_ID\", \"$comment->comment_ID\", \"$respond_id\", \"$post->ID\")'><span>$reply_text</span></a></li>";
		return apply_filters('comment_reply_link', $before . $link . $after, $args, $comment, $post);
	}
	function comment_reply_link_own($args = array(), $comment = null, $post = null) {
		echo get_comment_reply_link_own($args, $comment, $post);
	}

	// Own Comment Form
	function comment_form_own( $args = array(), $post_id = null ) {
		global $id;

		if ( null === $post_id )
			$post_id = $id;
		else
			$id = $post_id;

		$commenter = wp_get_current_commenter();
		$user = wp_get_current_user();
		$user_identity = $user->exists() ? $user->display_name : '';

		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$fields =  array(
			'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
			            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
			'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
			            '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
			'url'    => '<p class="comment-form-url"><label for="url">' . __( 'Website' ) . '</label>' .
			            '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>',
		);

		$required_text = sprintf( ' ' . __('Required fields are marked %s'), '<span class="required">*</span>' );
		$defaults = array(
			'fields'               => apply_filters( 'comment_form_default_fields', $fields ),
			'comment_field'        => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
			'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
			'logged_in_as'         => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), get_edit_user_link(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
			'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.' ) . ( $req ? $required_text : '' ) . '</p>',
			'comment_notes_after'  => '<p class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
			'id_form'              => 'commentform',
			'id_submit'            => 'submit',
			'title_reply'          => __( 'Leave a Reply' ),
			'title_reply_to'       => __( 'Leave a Reply to %s' ),
			'cancel_reply_link'    => __( 'Cancel reply' ),
			'label_submit'         => __( 'Post Comment' ),
		);

		$args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

		?>
			<?php if ( comments_open( $post_id ) ) : ?>
				<?php do_action( 'comment_form_before' ); ?>
				<div id="respond">
					<h3 class="icon-response-big" id="reply-title"><?php comment_form_title( $args['title_reply'], $args['title_reply_to'] ); ?> <small><?php cancel_comment_reply_link( $args['cancel_reply_link'] ); ?></small></h3>
					<?php if ( get_option( 'comment_registration' ) && !is_user_logged_in() ) : ?>
						<?php echo $args['must_log_in']; ?>
						<?php do_action( 'comment_form_must_log_in_after' ); ?>
					<?php else : ?>
						<form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" method="post" id="<?php echo esc_attr( $args['id_form'] ); ?>">
							<?php do_action( 'comment_form_top' ); ?>
							<?php if ( is_user_logged_in() ) : ?>
								<?php echo apply_filters( 'comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity ); ?>
								<?php do_action( 'comment_form_logged_in_after', $commenter, $user_identity ); ?>
							<?php else : ?>
								<?php echo $args['comment_notes_before']; ?>
								<?php
								do_action( 'comment_form_before_fields' );
								foreach ( (array) $args['fields'] as $name => $field ) {
									echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";
								}
								do_action( 'comment_form_after_fields' );
								?>
							<?php endif; ?>
							<?php echo apply_filters( 'comment_form_field_comment', $args['comment_field'] ); ?>
							<?php echo $args['comment_notes_after']; ?>
							<div class="buttonRow clearfix">
								<button name="submit" type="submit" id="<?php echo esc_attr( $args['id_submit'] ); ?>" class="button floatright"><span><?php echo esc_attr( $args['label_submit'] ); ?></span></button>
								<?php comment_id_fields( $post_id ); ?>
							</div>
							<?php do_action( 'comment_form', $post_id ); ?>
						</form>
					<?php endif; ?>
				</div><!-- #respond -->
				<?php do_action( 'comment_form_after' ); ?>
			<?php else : ?>
				<?php do_action( 'comment_form_comments_closed' ); ?>
			<?php endif; ?>
		<?php
	}

	// Own Content
	function the_content_own($more_link_text = null, $stripteaser = false) {
		$content = get_the_content_own($more_link_text, $stripteaser);
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]&gt;', $content);
		echo $content;
	}

	/**
	 * Retrieve the post content.
	 *
	 * @since 0.71
	 *
	 * @param string $more_link_text Optional. Content for when there is more text.
	 * @param bool $stripteaser Optional. Strip teaser content before the more text. Default is false.
	 * @return string
	 */
	function get_the_content_own( $more_link_text = null, $stripteaser = false ) {
		global $more, $page, $pages, $multipage, $preview;

		$post = get_post();

		if ( null === $more_link_text )
			$more_link_text = __( '(more...)' );

		$output = '';
		$hasTeaser = false;

		// If post password required and it doesn't match the cookie.
		if ( post_password_required() )
			return get_the_password_form();

		if ( $page > count($pages) ) // if the requested page doesn't exist
			$page = count($pages); // give them the highest numbered page that DOES exist

		$content = $pages[$page-1];
		if ( preg_match('/<!--more(.*?)?-->/', $content, $matches) ) {
			$content = explode($matches[0], $content, 2);
			if ( !empty($matches[1]) && !empty($more_link_text) )
				$more_link_text = strip_tags(wp_kses_no_null(trim($matches[1])));

			$hasTeaser = true;
		} else {
			$content = array($content);
		}
		if ( (false !== strpos($post->post_content, '<!--noteaser-->') && ((!$multipage) || ($page==1))) )
			$stripteaser = true;
		$teaser = $content[0];
		if ( $more && $stripteaser && $hasTeaser )
			$teaser = '';
		$output .= $teaser;
		if ( count($content) > 1 ) {
			if ( $more ) {
				$output .= '<span id="more-' . $post->ID . '"></span>' . $content[1];
			} else {
				if ( ! empty($more_link_text) )
					$output .= apply_filters( 'the_content_more_link', ' <div class="clearfix"><a href="' . get_permalink() . "\" class=\"button floatright\"><span>$more_link_text</span></a></div>", $more_link_text );
				$output = force_balance_tags($output);
			}

		}
		if ( $preview ) // preview fix for javascript bug with foreign languages
			$output =	preg_replace_callback('/\%u([0-9A-F]{4})/', '_convert_urlencoded_to_entities', $output);

		return $output;
	}
	function get_next_posts_link_own( $label = null, $max_page = 0 ) {
		global $paged, $wp_query;

		if ( !$max_page )
			$max_page = $wp_query->max_num_pages;

		if ( !$paged )
			$paged = 1;

		$nextpage = intval($paged) + 1;

		if ( null === $label )
			$label = __( 'Next Page &raquo;' );

		if ( !is_single() && ( $nextpage <= $max_page ) ) {
			$attr = apply_filters( 'next_posts_link_attributes', '' );
			return '<a class="button" href="' . next_posts( $max_page, false ) . "\" $attr>" . preg_replace('/&([^#])(?![a-z]{1,8};)/i', '&#038;$1', $label) . '</a>';
		}
	}

	function next_posts_link_own( $label = null, $max_page = 0 ) {
		echo get_next_posts_link_own( $label, $max_page );
	}

	function get_previous_posts_link_own( $label = null ) {
		global $paged;

		if ( null === $label )
			$label = __( '&laquo; Previous Page' );

		if ( !is_single() && $paged > 1 ) {
			$attr = apply_filters( 'previous_posts_link_attributes', '' );
			return '<a class="button" href="' . previous_posts( false ) . "\" $attr>". preg_replace( '/&([^#])(?![a-z]{1,8};)/i', '&#038;$1', $label ) .'</a>';
		}
	}

	function previous_posts_link_own( $label = null ) {
		echo get_previous_posts_link_own( $label );
	}

	
?>