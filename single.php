<?php get_header(); ?>
	<?php //get_template_part( 'includes/breadcrumb' , 'single'); ?>

	<div id="mainArea" class="col-9">
	<?php while ( have_posts() ) : the_post(); ?>
		
		<?php // get loop.php ?>
		<?php get_template_part( 'includes/post-full-loop' , 'single'); ?>

		<?php wp_link_pages(array('before' => '<p><strong>'.__('Pages:','themify').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

		<?php // get comment template (comments.php) ?>
		<?php comments_template(); ?>
			
	<?php endwhile; ?>
	</div>
	<!-- /#content -->


	<?php get_sidebar(); ?>
	
<?php get_footer(); ?>