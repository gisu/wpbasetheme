      </div><!-- .contentStage -->
      <div id="footerIndicator"> </div>
    </div><!-- #topWrapper-->
    <div id="bottomWrapper" class="container">
      <footer id="siteFooter" class="row">
        <div class="col-8">
          <h6>Meta</h6>
          <nav>
            <?php wp_nav_menu(array('theme_location' => 'footer-nav' , 'fallback_cb' => '' , 'container'  => '' , 'menu_id' => 'footer-nav' , 'menu_class' => 'floatingNavigation')); ?>
          </nav>
        </div>

        <div class="col-4">
          <div class="copyright">&copy; Copyright <?php echo date( 'Y' ); ?> Company</div>
        </div>
      </footer><!-- #siteFooter-->
    </div><!-- #bootomWrapper -->
    <script src="<?php echo get_template_directory_uri(); ?>/lib/js/essential/jquery.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/lib/js/polyfill/placeholder.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/lib/js/essential/conditionizr.js"></script>
    <script>
    $(function() {
      $('head').conditionizr({
        debug      : true,
        scriptSrc  : '<?php echo get_template_directory_uri(); ?>/lib/js/essential/',
        styleSrc   : 'css/conditionizr/',
        ieLessThan : { active: true, version: '9', scripts: false, styles: true, classes: true, customScript: '<?php echo get_template_directory_uri(); ?>/lib/js/polyfill/selectvizr.js'},
        chrome     : { scripts: false, styles: false, classes: true, customScript: 'none' },
        safari     : { scripts: false, styles: false, classes: true, customScript: 'none' },
        opera      : { scripts: false, styles: false, classes: true, customScript: 'none' },
        firefox    : { scripts: false, styles: false, classes: true, customScript: 'none' },
        ie10       : { scripts: false, styles: false, classes: true, customScript: 'none' },
        ie9        : { scripts: false, styles: false, classes: true, customScript: 'none' },
        ie8        : { scripts: false, styles: false, classes: true, customScript: '<?php echo get_template_directory_uri(); ?>/lib/js/polyfill/respond.js' },
        ie7        : { scripts: false, styles: false, classes: true, customScript: 'none' },
        ie6        : { scripts: false, styles: false, classes: true, customScript: 'none' },
        retina     : { scripts: false, styles: false, classes: true, customScript: 'none' },
        mac    : true,
        win    : true,
        x11    : true,
        linux  : true
      });
    });
    </script>
    <script src="<?php echo get_template_directory_uri(); ?>/lib/js/essential/app.js"></script>

   	<?php wp_footer(); ?>
  </body>
</html>