<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?php if (is_home() || is_front_page()) { echo bloginfo('name'); } else { echo wp_title(''); } ?></title>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php echo bloginfo('rss2_url'); ?>">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>">
<link rel="shortcut icon" type="images/png" href="<?php echo get_template_directory_uri(); ?>/lib/images/favicon.png">
<link rel="icon" type="images/png" href="<?php echo get_template_directory_uri(); ?>/lib/images/favicon.png">
<script src="<?php echo get_template_directory_uri(); ?>/lib/js/essential/modernizr.js"></script>
<?php wp_head(); ?>
<?php // enqueue comment-reply.js (require for threaded comments)
	if ( is_singular() && get_option( 'thread_comments' ) )	wp_enqueue_script( 'comment-reply' ); 
?>
</head>
<body <?php body_class($class); ?>>
<div id="topWrapper" class="container">            
  <header id="siteHeader" role="banner" class="row">
    <div id="subhead">
      <div class="row">
        <?php get_search_form(); ?>
        <nav id="softNavigation">
          <?php wp_nav_menu(array('theme_location' => 'soft-nav' , 'fallback_cb' => 'default_main_nav' , 'container'  => '' , 'menu_id' => 'soft-nav' , 'menu_class' => 'nav pullright')); ?>
        </nav>
      </div>
    </div>
    <h1 id="siteLogo">
    	<a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a>
    	<span class="slogan"><?php bloginfo('description'); ?></span>
    </h1>
    <nav id="mainNav" role="navigation">
	  <a href="#" class="mobileNavButton hiddenToPhone"> </a>
  		<?php wp_nav_menu(array('theme_location' => 'main-nav' , 'fallback_cb' => 'default_main_nav' , 'container'  => '' , 'menu_id' => 'main-nav' , 'menu_class' => 'nav floatright')); ?>
    </nav>
  </header>
  <div id="contentStage" class="row"> 
    <a href="#siteHeader" class="scrollTop blue-arrow-up">Zum Seitenanfang</a>
    
