<article id="post-<?php the_ID(); ?>" <?php post_class("searchPost $class"); ?>>
  <!-- .postMeta-->
  <div class="postContent">
    <header>
      <time datetime="<?php the_time('o-m-d') ?>"><?php the_time('j.m.Y') ?></time>
      <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </header>
    <?php the_excerpt_dynamic(200); ?>
  </div>
</article>