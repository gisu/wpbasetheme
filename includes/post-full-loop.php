<article id="post-<?php the_ID(); ?>" <?php post_class("fullPost postStyle clearfix $class"); ?>>
  <div class="postMeta">
    <div class="dateBlock">
      <div class="day"><time datetime="<?php the_time('o-m-d') ?>"><?php the_time('d') ?></time></div>
      <div class="month"><span class="dot"> </span><?php the_time('F Y') ?></div>
    </div>
    <div class="postCategory metaList">
      <div class="info"><span>Kategorien:</span></div>
      <ul class="nav comma">
      	<?php
      	 foreach((get_the_category()) as $category) {
	      	 echo '<li><a href="'.get_category_link($category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">';
	      	 echo $category->cat_name;
	      	 echo '</a></li>';
      	  }
      	?>
      </ul>
    </div>
  </div>
  <!-- .postMeta-->
  <div class="postContent">
    <div class="postAuthor">
       
      von <?php the_author_posts_link() ?> |  <a href="<?php comments_link(); ?>" class="icon-comment-small"><?php comments_number( __( '0 Kommentare', 'themify' ), __( '1 Kommentar', 'themify' ), __( '% Kommentare', 'themify' ) ); ?></a>
    </div>
    <header>
      <h2><?php the_title(); ?></h2>
    </header>
    <?php if ( has_post_thumbnail() ) : ?>
	<figure class="articleImage">
		<?php the_post_thumbnail(array(800,800)); ?>
	</figure> 
	<?php endif; ?>
    <?php the_content(); ?>


    <footer class="postFooter">

      <?php // Author Box ?>
      <div class="autorBox clearfix">
        <?php if (function_exists('get_avatar')) : ?>
          <figure class="authorImage floatleft">
            <?php echo get_avatar( get_the_author_email(), '100' ); ?>
          </figure>
        <div class="text floatright">
        <?php else: ?>
        <div>
        <?php endif; ?>
          <h5>&Uuml;ber den Autor <?php the_author(); ?></h5>
          <p><?php the_author_description(); ?></p>
          <a href="<?php echo get_author_posts_url( $author_id=$userID, $author_nicename = '' ); ?>" class="smallbutton whiteButton small-arrow-right"><span>Alle Artikel von <?php the_author(); ?> anzeigen</span></a>
        </div>
      </div>
      <?php // Author Box -> END ?>

      <div class="postFooterMeta">

        <?php // Tag List ?>
        <?php 
          $tag = get_the_tags();

          if ( $tag ) :
        ?>
        <div class="taglist metaList">
          <div class="info"> <span>Tags:</span></div>
          <?php the_tags('<ul class="nav comma"><li>','</li><li>','</li></ul>'); ?>
        </div>
        <?php endif; ?>
        <?php // Tag List -> END ?>

        <?php // Refering ?>
        <div class="referringSource metaList">
          <div class="info"> <span>Quellen:</span></div>
          <div class="text">
            <?php echo get_post_meta($post->ID, 'ecpt_sources', true);; ?>

          </div>
        </div>
        <?php // Refering -> END ?>

        <?php // Related Articles ?>
        <?php
          $tags = wp_get_post_tags($post->ID);
          if ($tags) {
            
            $first_tag = $tags[0]->term_id;
            $args=array(
              'tag__in' => array($first_tag),
              'post__not_in' => array($post->ID),
              'showposts'=>5,
              'caller_get_posts'=>1
             );
            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
              while ($my_query->have_posts()) : $my_query->the_post(); ?>
                <div class="relatedArticle metaList">
                  <div class="info"><span>&Auml;hnliche Artikel:</span></div>
                  <div class="text">
                <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
                <?php
              endwhile;
            echo '</div></div>';
            }
          }
        ?>
        <?php // Related Articles -> END ?>

      </div>
      <div class="clearfix"><a href="javascript:history.back()" class="whiteButton button floatright blue-arrow-left"><span>Zur&uuml;ck</span></a></div>
    </footer>
  </div>
</article>