<nav id="breadcrumb" class="col-12">
  <div class="sectionTitle">
  	<?php

    
    if ( !is_home() && !is_front_page() || is_paged() ) {
    

    
      global $post;
      $homeLink = get_bloginfo('url');
    
      if ( is_category() ) {
        global $wp_query;
        $cat_obj = $wp_query->get_queried_object();
        $thisCat = $cat_obj->term_id;
        $thisCat = get_category($thisCat);
        $parentCat = get_category($thisCat->parent);
        if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
        echo single_cat_title('', false);
      }
      elseif ( is_day() ) {
        echo 'Archiv: ';
        echo get_the_time('d');
        echo '.';
        echo get_the_time('m');
        echo ' ';
        echo get_the_time('Y');
   
      } elseif ( is_month() ) {
        echo 'Archiv: ';
        echo get_the_time('m');
        echo ' ';
        echo get_the_time('Y');
   
      } elseif ( is_year() ) {
        echo 'Archiv: ';
        echo get_the_time('Y');

      } elseif ( is_single() && !is_attachment() ) {
        if ( get_post_type() != 'post' ) {
          $post_type = get_post_type_object(get_post_type());
          $slug = $post_type->rewrite;
          echo $post_type->labels->singular_name ;        
        } else {
          $cat = get_the_category(); $cat = $cat[0];
          echo get_category_parents($cat, false, ' ' );
        }
      }
      elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
        if ( get_search_query() != ” ) {
          echo 'Suche';
        } else {
          $post_type = get_post_type_object(get_post_type());
          echo $post_type->labels->singular_name ;
        }
        

    
      } elseif ( is_page() && !$post->post_parent ) {
        echo get_the_title();

      } elseif ( is_search() ) {
        echo 'Suche';
      } elseif ( is_tag() ) {
        echo 'Tag';
      } elseif ( is_404() ) {
        echo 'Fehler 404';
      } 
    }

  	?>

  </div>
  <?php if (function_exists('nav_breadcrumb')) nav_breadcrumb(); ?>
</nav>