<article id="post-<?php the_ID(); ?>" <?php post_class("previewPost postStyle $class"); ?>>
  <div class="postMeta">
    <div class="dateBlock">
      <div class="day"><time datetime="<?php the_time('o-m-d') ?>"><?php the_time('d') ?></time></div>
      <div class="month"><span class="dot"> </span><?php the_time('F Y') ?></div>
    </div>
    <div class="postCategory metaList">
      <div class="info"><span>Kategorien:</span></div>
      <ul class="nav comma">
      	<?php
      	 foreach((get_the_category()) as $category) {
	      	 echo '<li><a href="'.get_category_link($category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">';
	      	 echo $category->cat_name;
	      	 echo '</a></li>';
      	  }
      	?>
      </ul>
    </div>
  </div>
  <!-- .postMeta-->
  <div class="postContent">
    <div class="postAuthor">
       
      von <?php the_author_posts_link() ?> |  <a href="<?php comments_link(); ?>" class="icon-comment-small"><?php comments_number( __( '0 Kommentare', 'themify' ), __( '1 Kommentar', 'themify' ), __( '% Kommentare', 'themify' ) ); ?></a>
    </div>
    <header>
      <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </header>
    <?php if ( has_post_thumbnail() ) : ?>
      <figure class="alignright alignresponse artimagepreview">
		    <?php the_post_thumbnail(array(250,250)); ?>
      </figure> 
	<?php endif; ?>
    <?php the_content_own('Weiterlesen'); ?>
  </div>
</article>