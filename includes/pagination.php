<?php 
	$pages = $wp_query->max_num_pages;
	if ($pages > 1) :
?>
<ul class="pager clearfix">
  <li class="floatleft"><?php next_posts_link_own('<i class="white-arrow-left leftIcon"></i><span>&Auml;ltere Beitr&auml;ge</span>') ?></li>
  <li class="floatright"><?php previous_posts_link_own('<span>Neuere Beitr&auml;ge</span><i class="white-arrow-right rightIcon"></i>') ?></li>
</ul>

<?php endif; ?>