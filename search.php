<?php get_header(); ?>		
	<?php get_template_part( 'includes/breadcrumb' , 'search'); ?>
	<div id="mainArea" class="col-9">
		
		<?php // the loop ?>
		<?php if (have_posts()) : ?>
		
			<?php while (have_posts()) : the_post(); ?>
	
				<?php get_template_part( 'includes/search-loop' , 'search'); ?>
	
			<?php endwhile; ?>
							
			<?php get_template_part( 'includes/pagination'); ?>
		
		<?php else : ?>
			<h2>Leider nichts gefunden</h2>
			<p>Leider wurde der gesuchte Begriff nicht gefunden, bitte verwenden Sie einen anderen Suchbegriff.</p>
		<?php endif; ?>			
	
	</div>
	<!-- /#content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>